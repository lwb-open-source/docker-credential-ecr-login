FROM golang:alpine3.8 as dependencies

RUN apk --no-cache add git \
  && go get -u github.com/awslabs/amazon-ecr-credential-helper/ecr-login/cli/docker-credential-ecr-login

FROM docker:stable

COPY --from=dependencies /go/bin/docker-credential-ecr-login /usr/bin/docker-credential-ecr-login
RUN chmod 777 /usr/bin/docker-credential-ecr-login
RUN apk add --no-cache curl